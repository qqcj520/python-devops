#!/usr/bin/python
# -*- coding: utf-8 -*-
import requests
import json
import sys
import os
  
headers = {'Content-Type': 'application/json;charset=utf-8'}
api_url = "https://oapi.dingtalk.com/robot/send?access_token=feaeffa086ca5eabecc5b8ae552913b3e600543ddc0ead0eae576c9c8c533766"
def msg(text):
    json_text= {
     "msgtype": "text",
     "text": {
         "content": text
     },
     "at": {
         "atMobiles": [
             "186..."
         ], 
         "isAtAll": False
     }
    }
    print requests.post(api_url,json.dumps(json_text),headers=headers).content
      
if __name__ == '__main__':
    text = sys.argv[1]
    msg(text)
