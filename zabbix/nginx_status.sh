#!/bin/bash
#active   当前活动的客户端连接数，包括waiting连接数
#accepts  已接受的客户端连接数
#handled  已处理的连接总数	
#requests 客户端请求的总数
#reading  正在读取请求头的当前连接数
#writing  将响应写回客户端的当前连接数
#waiting  等待请求空闲客户端的当前连接数

result="/usr/bin/curl -s http://116.63.63.138:8082/nginx_status"
case $1 in
    active)
        $result |awk '/Active/ {print $NF}'
    ;;
    accepts)
        $result |awk 'NR==3 {print $1}'
    ;;
    handled)
        $result |awk 'NR==3 {print $2}'
    ;;
    requests)
        $result |awk 'NR==3 {print $3}'
    ;;
    reading)
        $result |awk '/Reading/ {print $2}'
    ;;
    writing)
        $result |awk '/Writing/ {print $4}'
    ;;
    waiting)
         $result |awk '/Waiting/ {print $6}'
    ;;
          *)
    echo "USAGE:$0 {active|reading|writing|waiting|accepts|handled|requests}"
esac
