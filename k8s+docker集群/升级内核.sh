# 载入公钥
rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
# 安装ELRepo
rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm
# 载入elrepo-kernel元数据
yum --disablerepo=\* --enablerepo=elrepo-kernel repolist
# 查看可用的rpm包
yum --disablerepo=\* --enablerepo=elrepo-kernel list kernel*

# 安装最新版本的kernel
yum --disablerepo=\* --enablerepo=elrepo-kernel install -y kernel-ml.x86_64 -y
# 删除旧版本工具包
yum remove kernel-tools-libs.x86_64 kernel-tools.x86_64
# 安装新版本工具包
yum --disablerepo=\* --enablerepo=elrepo-kernel install -y kernel-ml-tools.x86_64

#更新kernel-ml-headers
wget https://elrepo.org/linux/kernel/el7/x86_64/RPMS/kernel-ml-headers-5.10.7-1.el7.elrepo.x86_64.rpm
rpm -ivh kernel-ml-headers-5.10.7-1.el7.elrepo.x86_64.rpm

#更新kernel-ml-devel
wget https://elrepo.org/linux/kernel/el7/x86_64/RPMS/kernel-ml-devel-5.10.7-1.el7.elrepo.x86_64.rpm
rpm -ivh kernel-ml-devel-5.10.7-1.el7.elrepo.x86_64.rpm


#查看所有内核启动 grub2
awk -F \' '$1=="menuentry " {print i++ " : " $2}' /etc/grub2.cfg
#修改为最新的内核启动
grub2-set-default 'CentOS Linux (5.10.7-1.el7.elrepo.x86_64) 7 (Core)'
#再次查看确认内核
grub2-editenv list
#重新启动
reboot