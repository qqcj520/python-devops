# 安装docker所需的工具  
yum install -y yum-utils device-mapper-persistent-data lvm2  
# 配置阿里云的docker源  
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo  
# 指定安装这个版本的docker-ce  
yum -y install docker-ce-18.06.1.ce-3.el7 
# 启动docker  
systemctl enable docker && systemctl start docker 
docker --version