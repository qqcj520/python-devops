#先更新软件包
yum -y update
#安装Docker虚拟机
yum install -y docker
#更换Docker Hub地址，境内阿里云加速
curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sudo sh -s https://rohpyfws.mirror.aliyuncs.com
#查看镜像
cat  /etc/docker/daemon.json

#启动，停止，重启
service docker start
service docker start
service docker stop
#查询，下载java镜像,改名
docker search java
docker pull docker.io/java
dockers tag nowname  newname
#导入、导出镜像
docker save java >/home/java.tart.gz
docker load </java.tart.gz
docker images

#启动镜像
docker run -it --name mjava java bash
docker run -it --name mjava java-p 9000:8080 -p 9001:8085 java bash
docker run -it --name mjava -v /home/project:/soft --privileged java bash
#暂停和停止容器
docker pause mjava
docker unpause mjava
docker stop mjava
docker start -i mjava
#删除容器和镜像
docker rm mjava
docker rmi java


#下载mysql镜像并启动
docker pull mysql:5.7
docker images
docker run -d --name mydb -v mysql:/var/lib/mysql -p 3307:3306 -e MYSQL_ROOT_PASSWORD=123456 mysql

#安装pxc镜像

docker pull percona/percona-xtradb-cluster:5.7.21

#创建docker 网络
 docker network create --subnet=172.18.0.0/16 net1
 docker inspect net1
 docker network rm net1
 docker inspect net1
 #创建docker卷
docker volume create --name v1
docker volume create --name v2
docker volume create --name v3
docker volume create --name v4
docker volume create --name v5
docker inspect v1
docker volume rm v1

#创建第1个MySQL节点
docker run -d -p 3306:3306 -e MYSQL_ROOT_PASSWORD=abc123456 -e CLUSTER_NAME=PXC -e XTRABACKUP_PASSWORD=abc123456 -v v1:/var/lib/mysql -v backup:/data --privileged --name=node1 --net=net1 --ip 172.18.0.2 pxc
#创建第2个MySQL节点
docker run -d -p 3307:3306 -e MYSQL_ROOT_PASSWORD=abc123456 -e CLUSTER_NAME=PXC -e XTRABACKUP_PASSWORD=abc123456 -e CLUSTER_JOIN=node1 -v v2:/var/lib/mysql -v backup:/data --privileged --name=node2 --net=net1 --ip 172.18.0.3 pxc
#创建第3个MySQL节点
docker run -d -p 3308:3306 -e MYSQL_ROOT_PASSWORD=abc123456 -e CLUSTER_NAME=PXC -e XTRABACKUP_PASSWORD=abc123456 -e CLUSTER_JOIN=node1 -v v3:/var/lib/mysql --privileged --name=node3 --net=net1 --ip 172.18.0.4 percona/percona-xtradb-cluster:5.7.21
#创建第4个MySQL节点
docker run -d -p 3309:3306 -e MYSQL_ROOT_PASSWORD=abc123456 -e CLUSTER_NAME=PXC -e XTRABACKUP_PASSWORD=abc123456 -e CLUSTER_JOIN=node1 -v v4:/var/lib/mysql --privileged --name=node4 --net=net1 --ip 172.18.0.5 pxc
#创建第5个MySQL节点
docker run -d -p 3310:3306 -e MYSQL_ROOT_PASSWORD=abc123456 -e CLUSTER_NAME=PXC -e XTRABACKUP_PASSWORD=abc123456 -e CLUSTER_JOIN=node1 -v v5:/var/lib/mysql -v backup:/data --privileged --name=node5 --net=net1 --ip 172.18.0.6 pxc
 
#安装haproxy
docker pull haproxy
vi /home/soft/haproxy.cfg
create user 'haproxy'@'%' IDENTIFIED BY '';
#启动Haproxy容器
docker run -it -d -p 4001:8888 -p 4002:3306 -v /home/soft/haproxy:/usr/local/etc/haproxy --name h1 --privileged --net=net1 --ip 172.18.0.7 haproxy
#进入h1容器，启动Haproxy
docker exec -it h1 bash
haproxy -f /usr/local/etc/haproxy/haproxy.cfg
#安装Keepalived 容器和宿主机  apt-get  设置为163加速

#安装redis集群
daemonize	yes
cluster-enabled	yes
cluster-config-file	nodes.conf
cluster-node-timeout 15000
appendonly yes